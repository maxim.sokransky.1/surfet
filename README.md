# Proxy server

The entry point of an Kubernetes cluster is an `ingress` service, that should allow to route http requests to desired applications.

`ingress` are still beta and have limited features, so to satisfy our needs we are using a home made `proxy` service behind GCE ingress.

`kube-rp` cluster will host many applications may be for many tenant/partners/organisations.

Let's say `app1`, `app2` and `app3` are deployed in the cluster respectively on both `staging` and `qa` environments.

The 2 first apps belong to tenant `t1`, the latter for `t2`.

* url to acces `app1` in staging will be: https://t1.siscc.org/staging/app1
* on `qa`: https://t1.siscc.org/qa/app1
* `app2` on `staging` https://t2.siscc.org/staging/app2

```
  https://<tenant>.siscc.org/<environment>/<application>
```

REM: `<tenant>.siscc.org` could be replaced by a dedicaded DNS entry in tenant's DNS pointing to `kube-rp` @IP

So we have 2 needs:

* route request depending on (`host`, `path`) urls
* set tenants headers depending on `host` to instruct target application of the tenant


## Docker Images

*siscc/dotstatsuite-proxy*

## Probe

`GET /_healthcheck_`

## Gitlab

https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-proxy/


## Usage

### Development

Clone repo

```
$ yarn
$ PORT=5007 yarn start:srv
```

## Settings


To add an access to a new application `app1` deployed in `staging` environment on `kube-rp`:

Add entries in `data/routes.json`

```
[
  {
    "host": "kube.tenant1.com",
    "target": "http://app1.staging.svc.cluster.local"
    "tenant": tenant1",
    "path": "/sfs"
    "forceHttps": true
  },
  ...
]
```

1. `kube.tenant1.com` is a tenan1's DNS entry linked to kube-rp@IP
3. `app1` in `target` must match a service name deployed in `staging`
4. `tenant` will be added to destination request to the target service, here `tenant1` may help to get `config` resources.
5. `forceHttps` will redirect if req.headers['x-forwarded-proto'] === 'http' CARE it only works behind a proxy




