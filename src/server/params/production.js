const server = { host: '0.0.0.0', port: process.env.PORT || 80 };
module.exports = {
  server,
  configUrl: process.env.CONFIG_URL,
};
