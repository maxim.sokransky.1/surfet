const server = { host: '0.0.0.0', port: process.env.PORT || 3007 };

module.exports = {
  server,
  configUrl: process.env.CONFIG_URL || 'http://localhost:5007',
};
