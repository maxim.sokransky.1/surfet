import { initHttp, getApp } from '../http';

const APPS = [
  {
    host: 'H1',
    tenant: 'T1',
    target: 'http://webapp',
  },
  {
    host: 'siscc-staging',
    path: '/sfs',
    tenant: 'siscc',
    target: 'http://app1',
  },
  {
    host: 'H2',
    tenant: 'T1',
    target: 'http://h3',
  },
  {
    host: 'H3',
    target: 'http://h3',
  },
  {
    host: 'H4',
    target: 'http://h3',
    forceHttps: true,
  },
];

const App = getApp(APPS);

describe('Router', () => {
  it('check1', () => {
    const req = { url: '/', headers: { host: 'H1' } };
    const app = App(req);
    const res = {
      headers: { 'x-tenant': 'T1' },
      target: 'http://webapp/',
    };
    expect(app).toEqual(res);
  });

  it('check2', () => {
    const req = { url: '?tenant=toto&a=b', headers: { host: 'H1' } };
    const app = App(req);
    const res = {
      headers: { 'x-tenant': 'T1' },
      target: 'http://webapp?a=b',
    };
    expect(app).toEqual(res);
  });

  it('check3', () => {
    const req = { url: '/sfs/report', headers: { host: 'siscc-staging' } };
    const app = App(req);
    const res = {
      headers: { 'x-tenant': 'siscc' },
      target: 'http://app1/report',
    };
    expect(app).toEqual(res);
  });

  it('check4', () => {
    const req = { url: '/sfs/report', headers: { host: 'H2' } };
    const app = App(req);
    const res = {
      headers: { 'x-tenant': 'T1' },
      target: 'http://h3/sfs/report',
    };
    expect(app).toEqual(res);
  });

  it('check5', () => {
    const req = { url: '?tenant=toto', headers: { host: 'H3' } };
    const app = App(req);
    const res = {
      target: 'http://h3?tenant=toto',
    };
    expect(app).toEqual(res);
  });

  it('check7', async () => {
    const req = { url: '?tenant=toto', headers: { host: 'H4' } };
    const app = await App(req);
    const res = {
      target: 'http://h3?tenant=toto',
      forceHttps: true,
    };
    expect(app).toEqual(res);
  });
});
