import http from 'http';
import axios from 'axios';
import initHttp, { getServerUrl } from '../http';

const runServer = cb => {
  return new Promise(resolve => {
    const configServer = http
      .createServer((req, res) => {
        cb(req, res);
      })
      .listen({ host: 'localhost', port: undefined }, () => resolve(configServer));
  });
};

describe('Assets', () => {
  it('Should request config server', async done => {
    let proxyServer;
    const configServer = await runServer((req, res) => {
      res.end();
      done();
      proxyServer.close();
      configServer.close();
    });

    const configUrl = getServerUrl(configServer);
    const config = { targetApps: [], configUrl, server: { host: 'localhost' } };
    initHttp({ config }).then(ctx => {
      proxyServer = ctx.server;
      axios.get(`${proxyServer.url}/assets/test`);
    });
  });
});
