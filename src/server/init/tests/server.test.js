import http from 'http';
import axios from 'axios';
import initHttp, { getServerUrl } from '../http';

const runServer = cb => {
  return new Promise(resolve => {
    const configServer = http
      .createServer((req, res) => {
        cb(req, res);
      })
      .listen({ host: 'localhost', port: undefined }, () => resolve(configServer));
  });
};

describe('Route', () => {
  it('Should request target app', async done => {
    let proxyServer;
    const appServer = await runServer((req, res) => {
      res.end();
      expect(req.headers['x-tenant']).toEqual(targetApps[0].tenant);
      done();
      proxyServer.close();
      appServer.close();
    });

    const targetApps = [
      {
        host: '127.0.0.1',
        target: getServerUrl(appServer),
        tenant: 'toto',
      },
    ];

    const configUrl = 'CONFIG_URL';
    const config = { targetApps, configUrl, server: { host: '127.0.0.1' } };
    initHttp({ config }).then(ctx => {
      proxyServer = ctx.server;
      axios.get(`${proxyServer.url}/test`);
    });
  });
});
