import debug from '../debug';
import params from '../params';

const targetApps = require('../../../data/routes');

const init = () => {
  debug.info(`running "${params.env}" env`);
  if (!params.configUrl) throw new Error('Cannot start config server without CONFIG_URL set!');
  return Promise.resolve({ config: { ...params, targetApps } });
};

export default init;
