import http from 'http';
import axios from 'axios';
import debug from '../debug';
import httpProxy from 'http-proxy';
import { slice, allPass, anyPass, find, replace, propEq } from 'ramda';
import urljoin from 'url-join';

const proxy = httpProxy.createProxyServer({
  xfwd: true,
  ws: true,
});

const notFound = (res, target) => err => {
  debug.warn(`cannot route to "${target}": ${err.code}`);
  res.statusCode = 404;
  res.end();
};

const matchPath = path => ({ url }) => new RegExp(`^${path}(\/|[?]|$)`).test(url);
const isAssetRequest = matchPath('/assets');
const isHealthcheckRequest = anyPass([matchPath('/_healthcheck_')]);
const makeTargetAsset = (configUrl, url) => urljoin(configUrl, url);

export const getServerUrl = server => `http://${server.address().address}:${server.address().port}`;
const matchAppHost = host => app => new RegExp(`^${app.host}`).test(host);
const matchAppPath = url => app => {
  if (!app.path) return true;
  return new RegExp(`^${app.path}`).test(url);
};

const findRoute = ({ url, host }, apps) => find(allPass([matchAppHost(host), matchAppPath(url)]), apps);

const redirectHttps = req => {
  if (req.headers['x-forwarded-proto'] === 'http') return `https://${req.headers.host}${req.url}`;
};

const makeTarget = ({ path, target, tenant }, url) => {
  let [newUrl, str] = url.split('?');
  const params = new URLSearchParams(str);
  if (tenant && params.has('tenant')) params.delete('tenant');
  if (path) newUrl = replace(new RegExp(`^${path}`), '', newUrl);
  const query = params.toString() ? `?${params.toString()}` : '';
  return urljoin(target, newUrl, query);
};

export const getApp = apps => ({ url, headers: { host } }) => {
  const app = findRoute({ url, host }, apps);
  if (!app) return;
  debug.info(app, 'matched route');
  const target = makeTarget(app, url);
  const res = { target, forceHttps: app.forceHttps };
  if (app.tenant) res.headers = { 'x-tenant': app.tenant };
  return res;
};

const startTime = new Date();
const init = ctx => {
  const {
    config: {
      targetApps,
      configUrl,
      server: { host, port },
    },
  } = ctx;
  const App = getApp(targetApps);
  return new Promise(resolve => {
    const server = http
      .createServer((req, res) => {
        const {
          headers: { host },
          url,
        } = req;

        try {
          if (isHealthcheckRequest(req)) {
            res.setHeader('Content-Type', 'application/json');
            const result = {
              status: 'OK',
              startTime,
              configUrl,
              gitHash: process.env.GIT_HASH,
            };
            axios
              .get(urljoin(configUrl, 'healthcheck'))
              .then(({ data }) => {
                res.write(JSON.stringify({ ...result, configServer: data }));
                res.statusCode = 200;
                res.end();
              })
              .catch(e => {
                debug.error(e);
                res.statusCode = 200;
                res.write(JSON.stringify({ ...result, configServer: 'KO' }));
                res.end();
              });
            return;
          }

          if (isAssetRequest(req)) {
            const target = makeTargetAsset(configUrl, url);
            debug.info(`route "${host}${url}" to "${target}"`);
            return proxy.web(req, res, { ignorePath: true, target }, notFound(res, target));
          }

          const app = App(req);

          if (!app) {
            debug.warn(`unknown target app for "${host}${url}"`);
            res.statusCode = 404;
            return res.end();
          }

          if (app.forceHttps) {
            const redirect = redirectHttps(req);
            if (redirect) {
              debug.info(`redirect "${req.hostname}${req.path}" to "${redirect}"`);
              res.writeHead(301, { Location: redirect });
              return res.end();
            }
          }

          if (app.headers?.['x-tenant'])
            debug.info(`route "${host}${url}" to "${app.target}" for tenant ${app.headers['x-tenant']}`);
          else debug.info(`route "${host}${url}" to "${app.target}"`);
          proxy.web(
            req,
            res,
            { ignorePath: true, target: app.target, headers: app.headers },
            notFound(res, app.target),
          );
        } catch (e) {
          debug.error(e);
          res.statusCode = 500;
          return res.end();
        }
      })
      .listen({ host, port });

    server.on('listening', () => {
      server.url = getServerUrl(server);
      debug.info('ready to proxy U ...');
      resolve({ ...ctx, server });
    });
  });
};

export default init;
