import debug from './debug';
import { reduce } from 'ramda';
import initHttp from './init/http';
import initConfig from './init/config';

const ressources = [initHttp];
const initRessources = ctx => reduce((acc, initFn) => acc.then(initFn), Promise.resolve(ctx), ressources);

initConfig()
  .then(initRessources)
  .then(({ config }) => {
    debug.info(config, 'running config');
    debug.info('🚀 server started');
  })
  .catch(err => debug.error(err));
